import java.lang.reflect.*;
import java.util.*;
public class Game{
	private ArrayList<ArrayList<Integer>> fsc = new ArrayList();
	private int score=0;
	private int frame_score=0;
	private int frame=0;
	private int shots=0;
	private int mulFactorState=0;
	private boolean match_end=false;
/*	
	0 normal
	1 spare 
	2 strike

*/
	
	public void roll(int pins){
		frame_score+=pins;
		shots++;

		printStatus();
		checkSpecial();
		if(shots==2){
			nextFrame();
		}
	}

	public void checkSpecial(){
		if(frame_score==10 && shots==2){
			System.out.println("spare");

			fsc.add(new ArrayList(){{add(10);add(1);}});

		}
		else if(frame_score==10 && shots==1){
			fsc.add(new ArrayList(){{add(10);add(2);}});
		}
		else if(shots==2){
			fsc.add(new ArrayList(){{add(frame_score);add(0);}});
		}
	}
	public int score(){
		return getScoreFor(0);		
	}

	public int getScoreFor(int frame){
		if(frame<=9){
			ArrayList<Integer> fp = fsc.get(frame);	
			if(fp.get(1)==0)
				return fp.get(0)+getScoreFor(frame++);

		}
		return 0;
	}



	public void nextFrame(){
		frame_score=0;
		shots=0;
	}

	public void printStatus(){
		System.out.println("Status");
		for(Field f: Game.class.getDeclaredFields()){
			f.setAccessible(true);
try{
			System.out.println("\t"+f.getName()+" : "+f.get(this));
}catch(Exception e){e.printStackTrace();}
		}

	}

}
